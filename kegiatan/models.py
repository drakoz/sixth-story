from django.db import models

class Peserta(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name
    objects = models.Manager()

class Kegiatan(models.Model):
    name = models.CharField(max_length=50)
    peserta = models.ManyToManyField(Peserta, default=1)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name
    objects = models.Manager()