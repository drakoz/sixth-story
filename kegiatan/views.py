from django.shortcuts import render
from .models import Kegiatan, Peserta

# Create your views here.
def index(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    context = {'kegiatan':kegiatan, 'peserta':peserta}
    return render(request, 'sixth_story/index.html', context)